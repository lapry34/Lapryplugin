package me.lapry;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

	private static Main instance;

	private static final Server bukkit = Bukkit.getServer();
	
	public static Main getInstance(){
		return instance;
	}
	
	public static Server getBukkit(){
		return bukkit;
	}
	
	@Override
	public void onEnable(){
	
		this.saveDefaultConfig();
		instance = this;
		getBukkit().getLogger().info("Exampleplugin was enabled");
	}
	
	@Override
	public void onDisable(){}
	
}
